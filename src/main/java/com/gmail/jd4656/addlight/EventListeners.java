package com.gmail.jd4656.addlight;

import com.sk89q.worldedit.math.BlockVector3;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import ru.beykerykt.lightapi.LightAPI;
import ru.beykerykt.lightapi.chunks.ChunkInfo;

import java.util.Iterator;
import java.util.List;

public class EventListeners implements Listener {
    private AddLight plugin;

    EventListeners(AddLight p) {
        plugin = p;
    }

    @EventHandler
    public void PlayerQuitEvent(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        plugin.players.remove(player.getUniqueId().toString());
    }

    @EventHandler
    public void changedWorld(PlayerChangedWorldEvent event) {
        Player player = event.getPlayer();
        plugin.players.remove(player.getUniqueId().toString());
    }

    @EventHandler
    public void PlayerInteractEvent(PlayerInteractEvent event) {
        if (event == null || event.getHand() == null || !event.getHand().equals(EquipmentSlot.HAND)) return;
        Player player = event.getPlayer();
        Block block = event.getClickedBlock();
        ItemStack item = event.getItem();
        Action action = event.getAction();

        if (block == null || item == null) return;
        if (!plugin.players.containsKey(player.getUniqueId().toString())) return;
        if (!item.getType().equals(Material.GLOWSTONE_DUST)) return;

        int lightLevel = plugin.players.get(player.getUniqueId().toString());

        if (action.equals(Action.LEFT_CLICK_BLOCK)) {
            event.setCancelled(true);
            if ((plugin.worldEdit != null) && plugin.isLocationInSelection(player, block.getLocation())) {
                Iterator<BlockVector3> blocks = plugin.getBlockVector(player, block.getLocation());

                while(blocks != null && blocks.hasNext())
                {
                    BlockVector3 pos = blocks.next();
                    Block targetBlock = block.getLocation().getWorld().getBlockAt(pos.getBlockX(), pos.getBlockY(), pos.getBlockZ());
                    if(targetBlock.getType() != Material.AIR) {
                        if (!player.isSneaking()) {
                            LightAPI.createLight(targetBlock.getLocation(), lightLevel, false);
                        } else {
                            LightAPI.deleteLight(targetBlock.getLocation(), false);
                        }
                    }
                }

                List<ChunkInfo> chunkInfos = LightAPI.collectChunks(player.getLocation());

                for (ChunkInfo chunkInfo : chunkInfos) {
                    LightAPI.updateChunk(chunkInfo);
                }
            } else {
                if (!player.isSneaking()) {
                    LightAPI.createLight(block.getLocation(), lightLevel, false);
                } else {
                    LightAPI.deleteLight(block.getLocation(), false);
                }

                List<ChunkInfo> chunkInfos = LightAPI.collectChunks(player.getLocation());

                for (ChunkInfo chunkInfo : chunkInfos) {
                    LightAPI.updateChunk(chunkInfo);
                }
            }
        }
    }
}
