package com.gmail.jd4656.addlight;

import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.bukkit.BukkitPlayer;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.regions.CylinderRegion;
import com.sk89q.worldedit.regions.Polygonal2DRegion;
import com.sk89q.worldedit.regions.Region;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class AddLight extends JavaPlugin {
    Map<String, Integer> players = new HashMap<>();
    WorldEditPlugin worldEdit;

    @Override
    public void onEnable() {
        worldEdit = (WorldEditPlugin) getServer().getPluginManager().getPlugin("WorldEdit");

        getServer().getPluginManager().registerEvents(new EventListeners(this), this);
        this.getCommand("addlight").setExecutor(new CommandAddLight(this));
        getLogger().info("AddLight loaded.");
    }

    boolean isLocationInSelection(Player player, Location location) {
        BukkitPlayer player2 = BukkitAdapter.adapt(player);
        LocalSession session = WorldEdit.getInstance().getSessionManager().get(player2);

        Region selection = null;
        try {
            selection = session.getSelection(player2.getWorld());
        } catch (IncompleteRegionException ignored) {}

        if (selection == null) return false;
        return selection.contains(BukkitAdapter.adapt(location).toVector().toBlockPoint());
    }

    Iterator<BlockVector3> getBlockVector(Player player, Location location) {
        if(!isLocationInSelection(player, location)) return null;
        BukkitPlayer bukkitPlayer = BukkitAdapter.adapt(player);

        Region region = null;
        try {
            region = WorldEdit.getInstance().getSessionManager().get(bukkitPlayer).getSelection(bukkitPlayer.getWorld());
        } catch (IncompleteRegionException ignored) {}

        // CuboidRegion
        if(region instanceof CuboidRegion)
        {
            CuboidRegion cuboid = (CuboidRegion)region;

            return cuboid.iterator();
        }
        else if(region instanceof Polygonal2DRegion)
        {
            Polygonal2DRegion polygonal2D = (Polygonal2DRegion)region;

            return polygonal2D.iterator();
        }
        else if (region instanceof CylinderRegion)
        {
            CylinderRegion cylinder = (CylinderRegion)region;

            return cylinder.iterator();
        }
        else
            return region != null ? region.iterator() : null;
    }
}
