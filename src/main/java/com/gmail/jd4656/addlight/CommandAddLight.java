package com.gmail.jd4656.addlight;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class CommandAddLight implements TabExecutor {
    private AddLight plugin;

    CommandAddLight(AddLight p) {
        plugin = p;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
            return true;
        }
        Player player = (Player) sender;

        if (!sender.hasPermission("addlight.use")) {
            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
            return true;
        }

        if (args.length < 1) {
            sender.sendMessage(ChatColor.DARK_GREEN + "AddLight commands:");
            sender.sendMessage(ChatColor.GOLD + "/addlight <on|enable> <light level (optional - defaults to 15)> - Enables the glowstone tool.");
            sender.sendMessage(ChatColor.GOLD + "/addlight <off|disable> - Disables the glowstone tool.");
            sender.sendMessage(ChatColor.GOLD + "/addlight <intensity> - Changes the light level.");
            return true;
        }

        if (args[0].equals("on") || args[0].equals("enable")) {
            int level = 15;

            if (plugin.players.containsKey(player.getUniqueId().toString())) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "The glowstone tool is already enabled.");
                return true;
            }

            if (args.length > 1) {
                try {
                    level = Integer.parseInt(args[1]);
                } catch (NumberFormatException e) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "\"" + args[1] + "\" is not a valid number.");
                    return true;
                }
                if (level < 1 || level > 15) {
                    sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "The light level may not be less than 1 or greater than 15.");
                    return true;
                }
            }

            plugin.players.put(player.getUniqueId().toString(), level);
            sender.sendMessage(ChatColor.GOLD + "AddLight has been enabled.");

            return true;
        }

        if (args[0].equals("intensity")) {
            if (args.length < 2) {
                sender.sendMessage(ChatColor.GOLD + "Usage: /addlight intensity <level> - Changes the light intensity level.");
                return true;
            }
            int level;

            if (!plugin.players.containsKey(player.getUniqueId().toString())) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "The glowstone tool is not enabled.");
                return true;
            }

            try {
                level = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "\"" + args[1] + "\" is not a valid number.");
                return true;
            }
            if (level < 1 || level > 15) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "The light level may not be less than 1 or greater than 15.");
                return true;
            }

            if (plugin.players.get(player.getUniqueId().toString()) == level) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "The light intensity level is already set to " + ChatColor.RED + level);
                return true;
            }

            plugin.players.replace(player.getUniqueId().toString(), level);
            sender.sendMessage(ChatColor.GOLD + "The light intensity level has been changed to " + ChatColor.RED + level);
            return true;
        }

        if (args[0].equals("off") || args[0].equals("disable")) {
            if (!plugin.players.containsKey(player.getUniqueId().toString())) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "The glowstone tool is not enabled.");
                return true;
            }

            plugin.players.remove(player.getUniqueId().toString());
            sender.sendMessage(ChatColor.GOLD + "The glowstone tool has been disabled.");
            return true;
        }

        sender.sendMessage(ChatColor.DARK_GREEN + "AddLight commands:");
        sender.sendMessage(ChatColor.GOLD + "/addlight <on|enable> <light level (optional - defaults to 15)> - Enables the glowstone tool.");
        sender.sendMessage(ChatColor.GOLD + "/addlight <off|disable> - Disables the glowstone tool.");
        sender.sendMessage(ChatColor.GOLD + "/addlight <intensity> - Changes the light level.");
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        List<String> tabComplete = new ArrayList<>();
        if (!sender.hasPermission("addlight.use")) return tabComplete;
        if (args.length == 1) {
            tabComplete.add("on");
            tabComplete.add("enable");
            tabComplete.add("off");
            tabComplete.add("disable");
            tabComplete.add("intensity");
        }

        if (args.length == 2 && args[0].equals("intensity")) tabComplete.add("<level (1-15)");

        return (args.length > 0) ? StringUtil.copyPartialMatches(args[args.length - 1], tabComplete, new ArrayList<>()) : null;
    }
}
